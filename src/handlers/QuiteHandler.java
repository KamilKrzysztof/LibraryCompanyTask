package handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

public class QuiteHandler {
	
	@Execute
	public void execute(IWorkbench iWorkbench ,Shell shell) {
		if(MessageDialog.openConfirm(shell, "Quit App", "Do you want to exit?")) {
			iWorkbench.close();
		}
	}
}
