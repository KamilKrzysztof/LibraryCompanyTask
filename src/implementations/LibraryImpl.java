package implementations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import api.LibraryAPI;
import helper.Bookstore;
import pojo.BookModel;

public class LibraryImpl implements LibraryAPI {

	public static String XML_LOCATION = "workspace/Library_Task/library.xml";

	private JAXBContext contextObj;
	private Marshaller marshallerObj;
	private Unmarshaller unmarshaller;

	private Bookstore bookstore = new Bookstore();
	private ArrayList<BookModel> bookList;

	public LibraryImpl() {
		try {
			contextObj = JAXBContext.newInstance(Bookstore.class);
			marshallerObj = contextObj.createMarshaller();
			unmarshaller = contextObj.createUnmarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		} catch (JAXBException e) {
			System.out.println("JAXB Problem: "+e);
		}
	}

	@Override
	public void addBook(BookModel book) {
		bookstore.addBook(book);
		try {
			marshallerObj.marshal(bookstore, new File(XML_LOCATION));
		} catch (JAXBException e) {
			System.out.println("JAXB Problem: " + e);
		}
	}

	@Override
	public void removeBookByID(int id) {
		bookstore.removeBook(id);
		try {
			marshallerObj.marshal(bookstore, new File(XML_LOCATION));
		} catch (JAXBException e) {
			System.out.println("JAXB Problem: " + e);
		}
	}

	@Override
	public void changeStatusBook(BookModel book) {
		BookModel tmpBook;
		if (!book.getStatusBook()) {
			book.setStatusBook(true);
		} else {
			book.setStatusBook(false);
		}
		tmpBook = book;
		removeBookByID(book.getIdBook());
		addBook(tmpBook);
	}

	@Override
	public ArrayList<BookModel> getBooks() {
		bookList = new ArrayList<>();
		try {
			bookstore = (Bookstore) unmarshaller.unmarshal(new FileReader(XML_LOCATION));
			bookList = bookstore.getBooksList();
		} catch (FileNotFoundException e) {
			System.out.println("");
		} catch (JAXBException e) {
			System.out.println("JAXB Problem: " + e);
		}
		return bookList;
	}

	@Override
	public BookModel getBook(int id) {
		return bookList.get(id);
	}

	@Override
	public BookModel getBookById(int id) {
		BookModel book = new BookModel();
		bookList = getBooks();
		for (int i = 0; i < bookList.size(); i++) {
			int idNew = bookList.get(i).getIdBook();
			if (id == idNew) {
				book = bookList.get(i);
				return book;
			}
		}
		return book;
	}

	@Override
	public boolean checkBookStatus() {
		List<BookModel> tempBookList = new ArrayList<>();
		tempBookList = bookList;
		bookList = getBooks();

		for (int i = 0; i < bookList.size(); i++) {
			BookModel tempBookModel = tempBookList.get(i);
			BookModel listBookModel = bookList.get(i);
			if (tempBookModel.getStatusBook() == listBookModel.getStatusBook()) {
			} else {
				return true;
			}
		}
		return false;
	}

}
