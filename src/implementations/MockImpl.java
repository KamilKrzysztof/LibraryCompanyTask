package implementations;

import java.util.ArrayList;

import api.LibraryAPI;
import pojo.BookModel;

public class MockImpl implements LibraryAPI {

	private ArrayList<BookModel> bookList = new ArrayList<>();

	@Override
	public void addBook(BookModel book) {
		bookList.add(book);
	}

	@Override
	public void changeStatusBook(BookModel book) {
		BookModel tmpBook;
		if (!book.getStatusBook()) {
			book.setStatusBook(true);
		} else {
			book.setStatusBook(false);
		}
		tmpBook = book;
		removeBookByID(book.getIdBook());
		addBook(tmpBook);
	}

	@Override
	public ArrayList<BookModel> getBooks() {
		return bookList;
	}

	@Override
	public BookModel getBook(int id) {
		return bookList.get(id);
	}

	@Override
	public void removeBookByID(int id) {
		for (int i = 0; i < bookList.size(); i++) {
			if (bookList.get(i).getIdBook() == id) {
				bookList.remove(i);
			}
		}
	}

	@Override
	public BookModel getBookById(int id) {
		BookModel book = new BookModel();
		bookList = getBooks();
		for (int i = 0; i < bookList.size(); i++) {
			int idNew = bookList.get(i).getIdBook();
			if (id == idNew) {
				book = bookList.get(i);
				return book;
			}
		}
		return book;
	}

	@Override
	public boolean checkBookStatus() {
		return false;
	}

}
