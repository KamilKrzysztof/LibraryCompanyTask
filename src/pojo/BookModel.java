package pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "book")
public class BookModel{

	private int idBook;
	private String nameBook;
	private String authorBook;
	private int yearBook;
	private boolean statusBook;

	public BookModel() {
	}

	public BookModel(int idBook, String nameBook, String authorBook, int yearBook, boolean statusBook) {
		this.idBook = idBook;
		this.nameBook = nameBook;
		this.authorBook = authorBook;
		this.yearBook = yearBook;
		this.statusBook = statusBook;
	}

	@XmlAttribute
	public int getIdBook() {
		return idBook;
	}

	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}

	@XmlElement(name = "bookName")
	public String getNameBook() {
		return nameBook;
	}

	public void setNameBook(String nameBook) {
		this.nameBook = nameBook;
	}

	public String getAuthorBook() {
		return authorBook;
	}

	public void setAuthorBook(String authorBook) {
		this.authorBook = authorBook;
	}

	public int getYearBook() {
		return yearBook;
	}

	public void setYearBook(int yearBook) {
		this.yearBook = yearBook;
	}

	public boolean getStatusBook() {
		return statusBook;
	}

	public void setStatusBook(boolean statusBook) {
		this.statusBook = statusBook;
	}
}
