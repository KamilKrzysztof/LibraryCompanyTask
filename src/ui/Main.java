package ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import api.LibraryAPI;
import pojo.BookModel;

public class Main {

	public static String CONFIG_LOCATION = "workspace/Library_Task/config.properties";

	private File configFile = new File(CONFIG_LOCATION);
	private GridData gridData;
	private Text title, author, year, status;
	private Table table;

	private int id = 0;
	private int indexTable;
	private int indexListElements;

	private List<BookModel> bookList = new ArrayList<>();

	private LibraryAPI library;
	private String libraryClassName;
	private Button changeStatusBtn, deleteBtn, addBtn;

	@Inject
	UISynchronize sync;

	@PostConstruct
	public void createNewComposite(Composite parent) {

		library = getClassObject();

		GridLayout layout = new GridLayout(2, false);
		parent.setLayout(layout);

		createListSectionUI(parent);
		createAddSectionUI(parent);

		checkConfigFile();
		checkStatus();
	}

	private Job checkConfigFile() {
		Job checkConfig = new Job("Check config file") {
			protected IStatus run(IProgressMonitor monitor) {
				try {
					checkConfigFile(configFile);
					if (monitor.isCanceled()) {
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				} finally {
					schedule(4000);
				}
			}

		};
		checkConfig.setSystem(true);
		checkConfig.schedule();
		return checkConfig;
	}

	private void checkConfigFile(File configFile) {
		try {
			FileReader reader = new FileReader(configFile);
			Properties props = new Properties();
			props.load(reader);
			String configClass = props.getProperty("configClass");

			String nameOfClass;
			try {
				nameOfClass = Class.forName(configClass).getName();
				if (libraryClassName.equals(nameOfClass)) {
				} else {
					library = getClassObject();
					sync.syncExec(new Runnable() {
						@Override
						public void run() {
							MessageDialog.openInformation(Display.getDefault().getActiveShell(), "Configuration file",
									"Configuration file changed!");
							table.removeAll();
							try {
								refreshTable();
							} catch (Exception e) {
								System.out.println("Class not found");
							}
						}
					});
				}
			} catch (ClassNotFoundException e1) {
				System.out.println("Couldn't find class");
			}
			reader.close();
		} catch (FileNotFoundException ex) {
			System.out.print("file does not exist");
		} catch (IOException ex) {
			System.out.print("I/O error");
		}

	}

	private Job checkStatus() {
		Job status = new Job("Check status") {
			protected IStatus run(IProgressMonitor monitor) {
				try {
					checkBookStatus();
					if (monitor.isCanceled()) {
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				} finally {
					schedule(4000);
				}
			}

		};
		status.setSystem(true);
		status.schedule();
		return status;
	}

	private void checkBookStatus() {
		if (library.checkBookStatus()) {
			sync.syncExec(new Runnable() {

				@Override
				public void run() {
					MessageDialog.openInformation(Display.getDefault().getActiveShell(), "Book status",
							"The status of the book in the library has been changed!");
					table.removeAll();
					refreshTable();
				}
			});
		}

	}

	private LibraryAPI getClassObject() {

		try {
			FileReader reader = new FileReader(configFile);
			Properties props = new Properties();
			props.load(reader);
			String configClass = props.getProperty("configClass");

			try {
				Object libraryClass = null;
				try {
					libraryClassName = Class.forName(configClass).getName();
					libraryClass = Class.forName(configClass).newInstance();
				} catch (InstantiationException e) {
					System.out.println("Object cannot be instantiated - " + e);
				} catch (IllegalAccessException e) {
					System.out.println("Access not available - " + e);
					e.printStackTrace();
				}
				library = (LibraryAPI) libraryClass;

			} catch (ClassNotFoundException e1) {
				System.out.println("Couldn't find class");
			}
			reader.close();

		} catch (FileNotFoundException ex) {
			System.out.print("file does not exist");
		} catch (IOException ex) {
			System.out.print("I/O error");
		}
		return library;
	}

	private void createAddSectionUI(Composite parent) {
		Composite addComposite = new Composite(parent, SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		addComposite.setLayoutData(gridData);
		addComposite.setLayout(new GridLayout(2, true));

		createTitleLabel(addComposite);
		createAuthorLabel(addComposite);
		createYearLabel(addComposite);
		createStatusLabel(addComposite);

		gridData = new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1);
		addBtn = new Button(addComposite, SWT.PUSH);
		addBtn.setLayoutData(gridData);
		addBtn.setText("Add new book");
		addBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				id++;
				
				if (!title.getText().isEmpty() & !author.getText().isEmpty() & !year.getText().isEmpty()
						& !status.getText().isEmpty()) {

					String statusBook = "available";
					boolean statusValue = false;
					if (status.getText().equals(statusBook)) {
						statusValue = true;
					}
					BookModel book = new BookModel(id, title.getText(), author.getText(),
							Integer.parseInt(year.getText()), statusValue);
					library.addBook(book);
				} else {
				}
				table.removeAll();
				library.getBooks();
				refreshTable();
				cleanFields();
				changeStatusBtn.setEnabled(false);
			}

			private void cleanFields() {
				title.setText("");
				author.setText("");
				year.setText("");
				status.setText("");
			}
		});

		deleteBtn = new Button(addComposite, SWT.PUSH);
		deleteBtn.setLayoutData(gridData);
		deleteBtn.setText("Delete book");
		deleteBtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				library.removeBookByID(indexListElements);
				table.removeAll();
				refreshTable();
				changeStatusBtn.setEnabled(false);
			}
		});

		changeStatusBtn = new Button(addComposite, SWT.PUSH);
		changeStatusBtn.setLayoutData(gridData);
		changeStatusBtn.setText("Change Status");
		changeStatusBtn.setEnabled(false);
		changeStatusBtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				BookModel book = library.getBookById(indexListElements);
				library.changeStatusBook(book);
				table.removeAll();
				refreshTable();
			}
		});
	}

	private void createListSectionUI(Composite parent) {
		Composite listComposite = new Composite(parent, SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);

		listComposite.setLayoutData(gridData);
		listComposite.setLayout(new GridLayout(1, true));

		table = new Table(listComposite, SWT.BORDER | SWT.FULL_SELECTION);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.heightHint = 200;
		table.setLayoutData(data);

		String[] titles = { "Title", "Author", "Year", "Status" };
		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(table, SWT.NONE);
			column.setText(titles[i]);
			table.getColumn(i).pack();
		}

		refreshTable();

		table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				bookList = library.getBooks();

				indexTable = table.getSelectionIndex();
				indexListElements = bookList.get(indexTable).getIdBook();
				changeStatusBtn.setEnabled(true);
			}
		});

		for (int i = 0; i < titles.length; i++) {
			table.getColumn(i).pack();
		}

	}

	private void refreshTable() {
		try {
			bookList = library.getBooks();
		} catch (Exception e) {
			bookList = new ArrayList<>();
		}

		String statusNew;
		for (int i = 0; i < bookList.size(); i++) {
			TableItem item = new TableItem(table, SWT.NONE);
			int a = 0;
			item.setText(a++, library.getBook(i).getNameBook());
			item.setText(a++, library.getBook(i).getAuthorBook());
			item.setText(a++, String.valueOf(library.getBook(i).getYearBook()));
			
			if (library.getBook(i).getStatusBook()) {
				statusNew = "available";
			}else {
				statusNew = "loaned";
			}
			item.setText(a++, statusNew);
		}
	}

	private void createTitleLabel(Composite composite) {
		Label titleLabel = new Label(composite, SWT.NONE);
		titleLabel.setText("Title: ");
		title = new Text(composite, SWT.BORDER);
	}

	private void createAuthorLabel(Composite composite) {
		Label authorLabel = new Label(composite, SWT.NONE);
		authorLabel.setText("Author: ");
		author = new Text(composite, SWT.BORDER);
	}

	private void createYearLabel(Composite composite) {
		Label yearLabel = new Label(composite, SWT.NONE);
		yearLabel.setText("Year: ");
		year = new Text(composite, SWT.BORDER);
	}

	private void createStatusLabel(Composite composite) {
		Label statusLabel = new Label(composite, SWT.NONE);
		statusLabel.setText("Status: ");
		status = new Text(composite, SWT.BORDER);
	}

}
